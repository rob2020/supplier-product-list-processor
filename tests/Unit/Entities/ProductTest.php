<?php

namespace Tests\Unit\Entities;

use ProductListProcessor\Entities\Product;
use Tests\TestCase;

class ProductTest extends TestCase
{
    public function test_that_product_is_instantiated_with_the_correct_values()
    {
        $make = 'make value';
        $model = 'model value';
        $colour = 'colour value';
        $capacity = 'capacity value';
        $network = 'network value';
        $grade = 'grade value';
        $condition = 'condition value';

        $subject = new Product(
            $make,
            $model,
            $colour,
            $capacity,
            $network,
            $grade,
            $condition
        );

        $this->assertSame($make, $subject->getMake());
        $this->assertSame($model, $subject->getModel());
        $this->assertSame($colour, $subject->getColour());
        $this->assertSame($capacity, $subject->getCapacity());
        $this->assertSame($network, $subject->getNetwork());
        $this->assertSame($grade, $subject->getGrade());
        $this->assertSame($condition, $subject->getCondition());
    }

    public function test_that_to_array_returns_correct_values()
    {
        $make = 'make value';
        $model = 'model value';
        $colour = 'colour value';
        $capacity = 'capacity value';
        $network = 'network value';
        $grade = 'grade value';
        $condition = 'condition value';

        $expectedReturnValue = [
            'make' => $make,
            'model' => $model,
            'colour' => $colour,
            'capacity' => $capacity,
            'network' => $network,
            'grade' => $grade,
            'condition' => $condition,
        ];

        $subject = new Product(
            $make,
            $model,
            $colour,
            $capacity,
            $network,
            $grade,
            $condition
        );

        $this->assertSame($expectedReturnValue, $subject->toArray());
    }

    /**
     * @dataProvider blankDataProvider
     *
     * @param $blankValue
     */
    public function test_that_exception_is_thrown_when_make_is_empty($blankValue)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Product(
            $blankValue,
            'model value',
            'colour value',
            'capacity value',
            'network value',
            'grade value',
            'condition value'
        );
    }

    /**
     * @dataProvider blankDataProvider
     *
     * @param $blankValue
     */
    public function test_that_exception_is_thrown_when_colour_is_empty($blankValue)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Product(
            'make value',
            $blankValue,
            'colour value',
            'capacity value',
            'network value',
            'grade value',
            'condition value'
        );
    }

    /**
     * @dataProvider blankDataProvider
     *
     * @param $blankValue
     */
    public function test_that_no_exception_is_thrown_when_non_required_properties_are_empty($blankValue)
    {
        $subject = new Product(
            'make value',
            'model value',
            $blankValue,
            $blankValue,
            $blankValue,
            $blankValue,
            $blankValue
        );

        $this->assertInstanceOf(Product::class, $subject);
    }

    /**
     * @return array
     */
    public function blankDataProvider()
    {
        return [
            [''],
            [' '],
            ['              '],
        ];
    }

    public function test_thing_get_unique_identifier_are_same_for_products_with_same_values()
    {
        $productA = new Product(
            'make value',
            'model value',
            'colour value',
            'capacity value',
            'network value',
            'grade value',
            'condition value'
        );

        $productB = new Product(
            'make value',
            'model value',
            'colour value',
            'capacity value',
            'network value',
            'grade value',
            'condition value'
        );

        $this->assertSame($productA->getUniqueIdentifier(), $productB->getUniqueIdentifier());
    }

    public function test_thing_get_unique_identifier_are_not_same_for_products_with_different_values()
    {
        $productA = new Product(
            'make value',
            'model value',
            'colour value',
            'capacity value',
            'network value',
            'grade value',
            'condition value'
        );

        $productB = new Product(
            'make value2',
            'model value2',
            'colour value2',
            'capacity value2',
            'network value2',
            'grade value2',
            'condition value2'
        );

        $this->assertNotSame($productA->getUniqueIdentifier(), $productB->getUniqueIdentifier());
    }
}
