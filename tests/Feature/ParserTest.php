<?php
namespace Tests\Feature;

use ProductListProcessor\Entities\Product;
use ProductListProcessor\Parsers\ProductParserFactory;
use ProductListProcessor\ProductListProcessorService;
use ProductListProcessor\ReportWriters\CsvReportWriter;
use SplFileObject;
use Tests\TestCase;

class ParserTest extends TestCase
{
    public function test_csv_parser_returns_expected_values()
    {
        $subject = $this->getProcessingService('products_comma_separated.csv');

        $productsIterator = $subject->getProducts();

        $this->assertIsIterable($productsIterator);
        $this->assertCount(5, $productsIterator);

        foreach ($productsIterator as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }

        $productsArray = iterator_to_array($productsIterator);


        $expectedProductOne = new Product('brand1', 'model1', 'color1', 'spec1', 'network1', 'grade1', 'condition1');
        $expectedProductTwo = new Product('brand2', 'model2', 'color2', 'spec2', 'network2', 'grade2', 'condition2');
        $expectedProductThree = new Product('brand3', 'model3', 'color3', 'spec3', 'network3', 'grade3', 'condition3');

        $this->assertSame($productsArray[0]->toArray(), $expectedProductOne->toArray());
        $this->assertSame($productsArray[1]->toArray(), $expectedProductOne->toArray());
        $this->assertSame($productsArray[2]->toArray(), $expectedProductTwo->toArray());
        $this->assertSame($productsArray[3]->toArray(), $expectedProductTwo->toArray());
        $this->assertSame($productsArray[4]->toArray(), $expectedProductThree->toArray());
    }

    public function test_tsv_parser_returns_expected_values()
    {
        $subject = $this->getProcessingService('products_tab_separated.tsv');

        $productsIterator = $subject->getProducts();

        $this->assertIsIterable($productsIterator);
        $this->assertCount(5, $productsIterator);

        foreach ($productsIterator as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }

        $productsArray = iterator_to_array($productsIterator);


        $expectedProductOne = new Product('brand1', 'model1', 'color1', 'spec1', 'network1', 'grade1', 'condition1');
        $expectedProductTwo = new Product('brand2', 'model2', 'color2', 'spec2', 'network2', 'grade2', 'condition2');
        $expectedProductThree = new Product('brand3', 'model3', 'color3' ,'spec3', 'network3', 'grade3', 'condition3');

        $this->assertSame($productsArray[0]->toArray(), $expectedProductOne->toArray());
        $this->assertSame($productsArray[1]->toArray(), $expectedProductOne->toArray());
        $this->assertSame($productsArray[2]->toArray(), $expectedProductTwo->toArray());
        $this->assertSame($productsArray[3]->toArray(), $expectedProductTwo->toArray());
        $this->assertSame($productsArray[4]->toArray(), $expectedProductThree->toArray());
    }

    private function getProcessingService(string $fileName): ProductListProcessorService
    {
        $inputFile = __DIR__ . '/data/' . $fileName;

        $parserFactory = new ProductParserFactory();
        $parser = $parserFactory->newParser($inputFile);

        return new ProductListProcessorService($parser);
    }

    public function test_csv_writer_writes_correct_data()
    {
        $subject = $this->getProcessingService('products_comma_separated.csv');

        $reportFile = __DIR__ . '/data/' . 'report.csv';

        $subject->generateCsvReport(new CsvReportWriter(__DIR__ . '/data/' . 'report.csv'));

        // We won't parse this as a csv as it's good to sees to check how the data is encoded
        $reportFile = new SplFileObject($reportFile);
        $reportFile->setFlags(
            SplFileObject::READ_AHEAD
            | SplFileObject::SKIP_EMPTY
            | SplFileObject::DROP_NEW_LINE
        );

        $reportArray = iterator_to_array($reportFile);

        $this->assertCount(4, $reportArray); // 3 results 1 header

        $this->assertSame('make,model,colour,capacity,network,grade,condition,count', $reportArray[0]);
        $this->assertSame('brand1,model1,color1,spec1,network1,grade1,condition1,2', $reportArray[1]);
        $this->assertSame('brand2,model2,color2,spec2,network2,grade2,condition2,2', $reportArray[2]);
        $this->assertSame('brand3,model3,color3,spec3,network3,grade3,condition3,1', $reportArray[3]);
    }

    public function test_tsv_writer_writes_correct_data()
    {
        $subject = $this->getProcessingService('products_tab_separated.tsv');

        $reportFile = __DIR__ . '/data/' . 'report.csv';

        $subject->generateCsvReport(new CsvReportWriter(__DIR__ . '/data/' . 'report.csv'));

        // We won't parse this as a csv as it's good to sees to check how the data is encoded
        $reportFile = new SplFileObject($reportFile);
        $reportFile->setFlags(
            SplFileObject::READ_AHEAD
            | SplFileObject::SKIP_EMPTY
            | SplFileObject::DROP_NEW_LINE
        );

        $reportArray = iterator_to_array($reportFile);

        $this->assertCount(4, $reportArray); // 3 results 1 header

        $this->assertSame('make,model,colour,capacity,network,grade,condition,count', $reportArray[0]);
        $this->assertSame('brand1,model1,color1,spec1,network1,grade1,condition1,2', $reportArray[1]);
        $this->assertSame('brand2,model2,color2,spec2,network2,grade2,condition2,2', $reportArray[2]);
        $this->assertSame('brand3,model3,color3,spec3,network3,grade3,condition3,1', $reportArray[3]);
    }
}
