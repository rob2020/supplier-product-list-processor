<?php

namespace ProductListProcessor;

use InvalidArgumentException;

class Assert
{
    /**
     * @param string $varName
     * @param $subject
     */
    public static function nonEmptyString(string $varName, $subject)
    {
        if (!is_string($subject)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s must be a string. '%s' provided.",
                    $varName,
                    getType($subject)
                )
            );
        }

        if (trim($subject) === '') {
            throw new InvalidArgumentException("$varName must be a string. '$subject' provided.");
        }
    }

    /**
     * @param string $varNameOne
     * @param string $varNameTwo
     * @param array $a
     * @param array $b
     */
    public static function arraysContainSameValues(string $varNameOne, string $varNameTwo, array $a, array $b)
    {
        asort($a);
        asort($b);

        $a = array_values($a);
        $b = array_values($b);

        if ($a !== $b) {
            throw new InvalidArgumentException(
                sprintf(
                    "$varNameOne and $varNameTwo must contain the same values"
                )
            );
        }
    }
}
