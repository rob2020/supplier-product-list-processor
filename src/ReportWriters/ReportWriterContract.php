<?php
namespace ProductListProcessor\ReportWriters;

use Iterator;

interface ReportWriterContract
{
    /**
     * IMPORTANT: It would be nicer if the method signature was `Product ...$products` however this will bring
     * all products in the iterator into memory at the same time.
     *
     * @param Iterator|\ProductListProcessor\Entities\Product[] $products
     */
    public function writeReport(Iterator $products) : void;
}
