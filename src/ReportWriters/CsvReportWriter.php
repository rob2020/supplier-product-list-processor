<?php
namespace ProductListProcessor\ReportWriters;

use \Iterator;
use ProductListProcessor\Entities\Product;
use SplFileObject;

class CsvReportWriter implements ReportWriterContract
{
    /** @var string */
    private $file;

    /**
     * CsvReportWriter constructor.
     *
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * IMPORTANT: It would be nicer if the method signature was `Product ...$products` however this will bring
     * all products in the iterator into memory at the same time.
     *
     * @param \Iterator $products
     * @param string $file
     */
    public function writeReport(Iterator $products): void
    {
        $file = new SplFileObject($this->file, 'w');

        // At this point we need to bring a decent chunk of the data into memory so that we can perform a count.
        // We'll try and minimize the size of the array by only storing the first instance of the product
        // and increment the count each time we see another
        $results = $this->buildReportArray($products);

        $this->writeToFile($file, $results);
    }

    /**
     * @param \Iterator $products
     * @return array
     */
    public function buildReportArray(Iterator $products): array
    {
        $results = [];
        foreach ($products as $product) {
            if (!$product instanceof Product) {
                throw new \InvalidArgumentException(
                    'All items in $products Iterator must be of a Product object'
                );
            }

            $uniqueIdentifier = $product->getUniqueIdentifier();

            if (!array_key_exists($uniqueIdentifier, $results)) {
                $result = $product->toArray();
                $result['count'] = 0;
                $results[$uniqueIdentifier] = $result;
            }

            $results[$uniqueIdentifier]['count']++;
        }

        return $results;
    }

    /**
     * @param \SplFileObject $file
     * @param array $results
     */
    public function writeToFile(SplFileObject $file, array $results): void
    {
        $isFirstRow = true;
        foreach ($results as $fields) {
            // Add the header once as the first row
            if ($isFirstRow) {
                $file->fputcsv(array_keys($fields));
                $isFirstRow = false;
            }

            $file->fputcsv($fields);
        }
    }
}
