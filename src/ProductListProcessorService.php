<?php
namespace ProductListProcessor;

use ProductListProcessor\Iterators\Iterator;
use ProductListProcessor\Parsers\ProductParserContract;
use ProductListProcessor\ReportWriters\ReportWriterContract;

class ProductListProcessorService
{
    /**
     * @var \ProductListProcessor\Iterators\Iterator
     */
    private $productIterator;

    public function __construct(ProductParserContract $productParser)
    {
        $this->productIterator = $productParser->parse();
    }


    public function getProducts() : Iterator
    {
        return $this->productIterator;
    }

    /**
     * @param \ProductListProcessor\ReportWriters\ReportWriterContract $reportWriter
     */
    public function generateCsvReport(ReportWriterContract $reportWriter)
    {
        $reportWriter->writeReport($this->productIterator);
    }
}
