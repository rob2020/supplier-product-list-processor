<?php
namespace ProductListProcessor;

class CliArgumentParser
{
    /**
     * @param array $arguments
     *
     * @return array
     */
    public static function parse(array $arguments)
    {
        $results = [];
        $i = 0;
        foreach (array_values($arguments) as $argument) {
            if (strpos($argument, '--') === 0) {
                $delimiterPosition = strpos($argument, '=');

                if ($delimiterPosition !== false) {
                    $results = self::assignEqualSignDelineatedValues($argument, $delimiterPosition, $results);
                } elseif (isset($arguments[$i + 1])) {
                    $results[substr($argument, 2)] = $arguments[$i + 1];
                }
            }

            $i++;
        }

        return $results;
    }

    /**
     * @param $argument
     * @param $delimiterPosition
     * @param array $results
     * @return array
     */
    private static function assignEqualSignDelineatedValues($argument, $delimiterPosition, array $results): array
    {
        $results[substr($argument, 2, $delimiterPosition - 2)] = substr(
            $argument,
            $delimiterPosition + 1
        );
        return $results;
    }
}
