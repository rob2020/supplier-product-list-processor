<?php
namespace ProductListProcessor\Iterators;

use ProductListProcessor\Assert;
use ProductListProcessor\Entities\Product;

class CsvIterator implements Iterator
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var \SplFileObject
     */
    private $fileIterator;

    /**
     * @var array
     */
    private $columnPositionMap;

    /**
     * CsvProductIterator constructor
     *
     * @param \SplFileObject $fileIterator
     */
    public function __construct(\SplFileObject $fileIterator)
    {
        $headers = $fileIterator->current();
        $fileIterator->next();

        $expectedHeaders = [
            "brand_name",
            "model_name",
            "colour_name",
            "gb_spec_name",
            "network_name",
            "grade_name",
            "condition_name",
        ];

        Assert::arraysContainSameValues('$expectedHeaders', '$headers', $expectedHeaders, $headers);

        // For performance reasons we've going to do this once and store it as a property of the class rather than on
        // each call to current.
        $this->columnPositionMap = array_flip($headers);

        $this->fileIterator = $fileIterator;

        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
        $this->fileIterator->rewind();

        // we need to skip past the headers again so we call next
        $this->fileIterator->next();
    }

    /**
     * @return \ProductListProcessor\Entities\Product
     */
    public function current() : Product
    {
        $data = $this->fileIterator->current();

        // We use the map we generated earlier to work out which column each value lives in. This means if the order of
        // the csv changes we don't need to change the code
        return new Product(
            $data[$this->columnPositionMap['brand_name']],
            $data[$this->columnPositionMap['model_name']],
            $data[$this->columnPositionMap['colour_name']],
            $data[$this->columnPositionMap['gb_spec_name']],
            $data[$this->columnPositionMap['network_name']],
            $data[$this->columnPositionMap['grade_name']],
            $data[$this->columnPositionMap['condition_name']]
        );
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * return void
     */
    public function next()
    {
        ++$this->position;
        $this->fileIterator->next();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return !$this->fileIterator->eof();
    }
}
