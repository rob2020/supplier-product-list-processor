<?php
namespace ProductListProcessor\Iterators;

use ProductListProcessor\Entities\Product;

interface Iterator extends \Iterator
{
    /**
     * @return \ProductListProcessor\Entities\Product
     */
    public function current(): Product;
}
