<?php
namespace ProductListProcessor\Parsers;

use SplFileObject;

class ProductParserFactory
{
    const CSV = 'csv';
    const TSV = 'tsv';

    /**
     * @param string $fileName
     *
     * @return \ProductListProcessor\Parsers\ProductParserContract
     */
    public function newParser(string $fileName) : ProductParserContract
    {
        $file = new SplFileObject($fileName);

        // We've newing up the same class with different args due to the similarity between csv and tsv but this could
        // easily return an another class that adheres to ProductParserContract for json or xml
        switch ($file->getExtension()) {
            case self::CSV:
                return new CsvProductParser($file, ',');

            case self::TSV:
                return new CsvProductParser($file, "\t");

            default:
                throw new \RuntimeException('Unsupported file type');
        }
    }
}
