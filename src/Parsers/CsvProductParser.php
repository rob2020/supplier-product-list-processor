<?php
namespace ProductListProcessor\Parsers;

use ProductListProcessor\Iterators\CsvIterator;
use SplFileObject;

class CsvProductParser implements ProductParserContract
{
    /** @var string */
    private $delimiter;

    /** @var \SplFileObject */
    private $csvFile;

    /**
     * CsvProductParser constructor.
     *
     * @param \SplFileObject $csvFile
     * @param string $delimiter
     */
    public function __construct(SplFileObject $csvFile, string $delimiter)
    {
        $this->delimiter = $delimiter;
        $this->csvFile = $csvFile;
    }

    /**
     * Parses a file and returns a Product iterator
     *
     * @return \ProductListProcessor\Iterators\CsvIterator
     */
    public function parse(): CsvIterator
    {
        $csvFile = $this->csvFile;

        $csvFile->setFlags(
            SplFileObject::READ_CSV
            | SplFileObject::READ_AHEAD
            | SplFileObject::SKIP_EMPTY
            | SplFileObject::DROP_NEW_LINE
        );

        $csvFile->setCsvControl($this->delimiter);

        // We could yield products directly from here instead of creating this Iterator partly did this to show that
        // I'm confident creating my own. In retro spec I wish I didn't
        return new CsvIterator($csvFile);
    }
}
