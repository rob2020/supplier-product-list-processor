<?php
namespace ProductListProcessor\Parsers;

use ProductListProcessor\Iterators\CsvIterator;

interface ProductParserContract
{
    /**
     * @return \ProductListProcessor\Iterators\CsvIterator
     */
    public function parse() : CsvIterator;
}
