<?php
namespace ProductListProcessor\Entities;

use ProductListProcessor\Assert;

class Product
{
    /** @var string */
    private $make;

    /** @var string */
    private $model;

    /** @var string */
    private $colour;

    /** @var string */
    private $capacity;

    /** @var string */
    private $network;

    /** @var string */
    private $grade;

    /** @var string */
    private $condition;

    /**
     * Product constructor.
     *
     * @param string $make
     * @param string $model
     * @param string $colour
     * @param string $capacity
     * @param string $network
     * @param string $grade
     * @param string $condition
     */
    public function __construct(
        string $make,
        string $model,
        string $colour,
        string $capacity,
        string $network,
        string $grade,
        string $condition
    ) {
        // We could add setters to this class and call them in the constructor but the logic is very simple so we'll
        // refactor later if the logic becomes more complex.
        Assert::nonEmptyString('$make', $make);
        Assert::nonEmptyString('$model', $model);

        $this->make = $make;
        $this->model = $model;
        $this->colour = $colour;
        $this->capacity = $capacity;
        $this->network = $network;
        $this->grade = $grade;
        $this->condition = $condition;
    }

    /**
     * @return string
     */
    public function getMake(): string
    {
        return $this->make;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getColour(): string
    {
        return $this->colour;
    }

    /**
     * @return string
     */
    public function getCapacity(): string
    {
        return $this->capacity;
    }

    /**
     * @return string
     */
    public function getNetwork(): string
    {
        return $this->network;
    }

    /**
     * @return string
     */
    public function getGrade(): string
    {
        return $this->grade;
    }

    /**
     * @return string
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'make' => $this->getMake(),
            'model' => $this->getModel(),
            'colour' => $this->getColour(),
            'capacity' => $this->getCapacity(),
            'network' => $this->getNetwork(),
            'grade' => $this->getGrade(),
            'condition' => $this->getCondition(),
        ];
    }

    /**
     * @return string
     */
    public function getUniqueIdentifier()
    {
        return implode('-', $this->toArray());
    }
}
