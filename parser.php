<?php

use ProductListProcessor\CliArgumentParser;
use ProductListProcessor\Parsers\ProductParserFactory;
use ProductListProcessor\ProductListProcessorService;
use ProductListProcessor\ReportWriters\CsvReportWriter;

require_once 'vendor/autoload.php';

$arguments = CliArgumentParser::parse($argv);

if (!array_key_exists('file', $arguments)) {
    print '--file argument is required';

    exit(1);
}

$parserFactory = new ProductParserFactory();
$parser = $parserFactory->newParser(__DIR__ . '/data/inputs/' . $arguments['file']);
$processingService = new ProductListProcessorService($parser);

// Print out parsed rows
foreach ($processingService->getProducts() as $product) {
    $results = [];
    foreach ($product->toArray() as $key => $value) {
        $results[] = ucfirst($key) . ': "' . $value . '"';
    }

    print implode(' | ', $results) . "\n";
}

if (array_key_exists('unique-combinations', $arguments)) {
    $processingService->generateCsvReport(
        new CsvReportWriter(__DIR__ . '/data/reports/' . $arguments['unique-combinations'])
    );
}
