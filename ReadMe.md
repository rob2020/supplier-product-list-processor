# Supplier Product List Processor

#### instruction:
Example usage:
```php parser.php --file=products_tab_separated.tsv --unique-combinations=combination_count.csv```

`--unique-combinations` is optional

Arguments can be supplied as `--file=products_comma_separated.csv` or `--file products_comma_separated.csv`

Input file need to be placed into ./data/inputs

Output will be written to ./data/reports

#### Design Decisions:
- Keep the csv data as a stream/iterator as much as possible to limit memory usage.

- When we get to the point where we need to create a count of unique product I decided to assign the first unique 
product into array keyed by a uniquer signature for that product. This increases the memory usage a bit but means we 
don't have to iterate over all the results to identify duplicates instead we just look for a key in the array.

- The `CsvProductParser` could return a generator by yielding a product instead of instantiating the custom iterator 
classes which would of simplified the code but I wanted to be able to rewind which can't be done with yield/generators.

- The `Product` class is very rigid in terms of what data it will accommodate, this would require a small refactor if the 
format of the csv changes. It could be made a bit more dynamic which would make any csv changes a bit easier
but because the data comes from third-parties I think it's a safer bet to validate the data as much as possible.

- The `ProductListProcessorService` class is only really their to act as a mediator between the parser, writer which could 
be done directly in the parser.php script. Decided to keep it as it's a good entry points for the feature tests + If 
this got developed in the future it would be a good place to handle error reporting and logging.

#### Improvements:
- More units test would be good.

- The parser could convert `Not Applicable` to a blank string which would save a bit of memory.

- Theirs scope to improve memory usage in in the `CsvReportWriter` class (seemed ok for now maxed out at 15mb for me)

- Could pass in some kind of logging class or call back function/callable into the the `CsvParser` that would print out 
the product details to the cli as it iterates over products instead of doing it separately in parser.php script like 
I'm doing a present.

- Currently the `ReportWriter` generates the reports and writes it to a csv. It could be good to separate the two 
things into their own classes which would make testing a bit easier.

- The report row is currently an array this could turned into a class of it's own.

- The `CliArgumentParser` needs a good refactor it's a bit hard to follow
